import unittest
import bovespa_parser

FILE_TEST_PATH = './resources/bovespa_teste_opcoes.DAT'

class TestPriceCandle(unittest.TestCase):

	def setUp(self):
		pass
	
	def test_parser(self):
		parser = bovespa_parser.OpcoesAbertoParser(FILE_TEST_PATH)
		#hard coded primeira linha do arquivo de teste 
		c01 = '01'
		c02 = 'CTIP' 
		c03 = 'ON      NM'
		c04 = '20141117'
		c05 =  477000
		c06 = 'PUT'
		c07 = 'CTIPW69'
		c08 = 'BRL'
		c09 =  1
		c10 =  19.68
		c11 = 0
		c12 = 61000
		c13 = 0
		c14 = 61000
		c15 = 2
		c16 = 2
		c17 = '121'
		c18 = '2'
		c19 = 'ACN'
		
		self.assertEqual(parser.registros[0].tipo_registro,c01)
		self.assertEqual(parser.registros[0].nome_emissora,c02)
		self.assertEqual(parser.registros[0].especificacao,c03)
		self.assertEqual(parser.registros[0].vencimento,c04)
		self.assertEqual(parser.registros[0].num_serie,c05)
		self.assertEqual(parser.registros[0].tipo_mercado,c06)
		self.assertEqual(parser.registros[0].ticker,c07)
		self.assertEqual(parser.registros[0].moeda,c08)
		self.assertEqual(parser.registros[0].fator_cotacao,c09)
		self.assertEqual(parser.registros[0].strike,c10)
		self.assertEqual(parser.registros[0].total_coberta,c11)
		self.assertEqual(parser.registros[0].total_travado,c12)
		self.assertEqual(parser.registros[0].descobertas,c13)
		self.assertEqual(parser.registros[0].total_opcoes,c14)
		self.assertEqual(parser.registros[0].total_titulares,c15)
		self.assertEqual(parser.registros[0].total_lancador,c16)
		self.assertEqual(parser.registros[0].distribuicao,c17)
		self.assertEqual(parser.registros[0].estilo,c18)
		self.assertEqual(parser.registros[0].isin,c19)
		
		
		
		
if __name__ == '__main__':
	unittest.main()