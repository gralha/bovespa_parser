# -*- coding: latin-1 -*-

import sys
#import datetime

TAMANHO_LINHA_REGISTRO = 161
#codigos do campo 06
COD_VENDA = '080'
COD_COMPRA = '070'
#codigos do campo 08
COD_INDIC_MOEDA_REAL = '0'
COD_DOLAR_DESPROTEGIDO = '1' 
COD_DOLAR_PROTEGIDO = '5'
COD_REFERENCIADA = '6'
#codigo do campo 19
COD_TIPO_INDICE = 'IND'
			
class ParserError(Exception):
	"""
	Exception do modulo de parse. Ocorre quando valores nao previstos aparecem no arquivo de
	Posicoes em aberto no mercado de Opções da BMF/Bovespa.
	"""
	
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

class BovespaRegistro(object):
	"""
	Uma classe vazia que funciona como estrutura de dados para armazenar cada linha do arquivo 
	de Posicoes em Aberto no mercado de Opções da BMF/Bovespa
	"""
	def __init__(self):
		pass
		
class OpcoesAbertoParser(object):
	"""
	Faz o parse do arquivo de dados estrutura de dados para armazenar cada linha do arquivo 
	de Posicoes em Aberto no Mercado de Opções da BMF/Bovespa
	"""
	def __init__(self,file_path):
		self.path = file_path
		self.registros = []
		self.__parser()
	
	def __make_header(self,separador=';'):
		"""
		Monta o header para um arquivo output csv. Segue a ordem dos campos conforme definido 
		no arquivo de Layout da BMF/Bovespa no dia 01/05/2014
		"""
		campos   = '01 tipo de registro'+separador 
		campos += '02 nome da sociedade emissora'+separador 
		campos += '03 especificacao do papel'+separador 
		campos += '04 vencimento'+separador 
		campos += '05 numero de serie'+separador 
		campos += '06 tipo de mercado'+separador 
		campos += '07 ticker da opcao'+separador 
		campos += '08 moeda'+separador 
		campos += '09 fator de cotacao'+separador 
		campos += '10 strike'+separador 
		campos += '11 total de posicoes cobertas'+separador 
		campos += '12 total de posicoes travadas'+separador 
		campos += '13 total de posicoes descobertas'+separador 
		campos += '14 total de opcoes'+separador 
		campos += '15 total de clientes titulares'+separador 
		campos += '16 total de clientes lancadores'+separador 
		campos += '17 distribuicao'+separador 
		campos += '18 estilo'+separador 
		campos += '19 codigo isin - ISO 6166' + '\n'
		return(campos)
	
	def __make_line(self,registro,separador=';'):
		"""
		Recebe um registro e tranforma em uma string, seguindo a ordem dos campos conforme definido no arquivo 
		de Layout da BMF/Bovespa no dia 01/05/2014. Cada atributo do registro é separado pelo separador.
		"""
		linha_csv =  registro.tipo_registro
		linha_csv += separador
		linha_csv +=  registro.nome_emissora
		linha_csv += separador
		linha_csv +=  registro.especificacao
		linha_csv += separador
		linha_csv +=  registro.vencimento
		linha_csv += separador
		linha_csv +=  str(registro.num_serie)
		linha_csv += separador
		linha_csv +=  registro.tipo_mercado
		linha_csv += separador
		linha_csv +=  registro.ticker
		linha_csv += separador
		linha_csv +=  registro.moeda
		linha_csv += separador
		linha_csv +=  str(registro.fator_cotacao)
		linha_csv += separador
		linha_csv +=  str(registro.strike)
		linha_csv += separador
		linha_csv +=  str(registro.total_coberta)
		linha_csv += separador
		linha_csv +=  str(registro.total_travado)
		linha_csv += separador
		linha_csv +=  str(registro.descobertas)
		linha_csv += separador
		linha_csv +=  str(registro.total_opcoes)
		linha_csv += separador
		linha_csv +=  str(registro.total_titulares)
		linha_csv += separador
		linha_csv +=  str(registro.total_lancador)
		linha_csv += separador
		linha_csv +=  str(registro.distribuicao)
		linha_csv += separador
		linha_csv +=  str(registro.estilo)
		linha_csv += separador
		linha_csv +=  registro.isin
		linha_csv += '\n'
		return (linha_csv)
	
	def save2csv(self,file_path,separador=';'):
		"""
		salva o arquivo bovespa num csv
		"""
		f = open(file_path,'w')
		header =  self.__make_header()
		f.write(header)
		for registro in self.registros:
			line = self.__make_line(registro)
			f.write(line)
		f.close()
	
	
	def __parser(self):
		"""
		salva o arquivo bovespa num csv
		"""
		
		f = open(self.path,'r')
		i = 1
		for line in f.readlines()[1:-1]:
			#print i
			if(len(line) != TAMANHO_LINHA_REGISTRO):
				msg = 'Tamanho da linha de registro e:'
				msg += len(line)+'devira ser: '+TAMANHO_LINHA_REGISTRO
				raise ParserError(msg)
			registro = BovespaRegistro()
			#campo 01 - fixo 01
			registro.tipo_registro = line[0:2]
			#campo 02 - nome da empresa emissora
			registro.nome_emissora = line[2:14].strip()
			#campo 03 - nao sei o que e
			registro.especificacao = line[14:24].strip()
			#campo 04 - data do vencimento AAAAMMDD
			#TODO: timestamp
			registro.vencimento = line[24:32].strip()
			#campo 05 - numero de serie
			registro.num_serie = int(line[32:39].strip())
			#campo 06 - tipo de opcao, compra ou venda
			if(line[39:42] == COD_VENDA):
				registro.tipo_mercado = "PUT"
			elif(line[39:42] == COD_COMPRA):
				registro.tipo_mercado = "CALL"
			else:
				raise ParserError('Foi encontrado na linha '+str(i)+' tipo mercado nao previsto: '+line[39:42])
			#campo 07 - codifo do papel
			registro.ticker = line[42:54].strip()
			#cmapo 08 - tipo de moeda (ou unidade) do registro
			if(line[54] == COD_INDIC_MOEDA_REAL):
				registro.moeda = 'BRL'
			#TODO: nao sei a diferenca entre dolar protegido e dolar desprotegido
			elif(line[54] == COD_DOLAR_DESPROTEGIDO):
				registro.moeda = 'USD'
			elif(line[54] == COD_DOLAR_PROTEGIDO):
				registro.moeda = 'USD_protegido'
			elif(line[54] == COD_DOLAR_DESPROTEGIDO and line[153:156] == COD_TIPO_INDICE):
				registro.moeda = 'USD'
			elif(line[54] == COD_REFERENCIADA and line[153:156] == COD_TIPO_INDICE):
				registro.moeda = 'Pontos do indice'
			else:
				raise ParserError('Foi encontrado na linha '+str(i)+' tipo mercado nao previsto: '+line[55:56])
			#campo 09 - nao sei o que e 
			registro.fator_cotacao = int(line[55:62])
			#campo 10 - preco do strike na moeda do registro
			registro.strike = float(line[62:75].strip())/100.
			#campo 11 - numero total de posicoes na opcao coberta. Em unidades da opcao
			registro.total_coberta = int(line[75:90].strip())
			#campo 12 - numero total de travas. Em unidades da opcao
			registro.total_travado = int(line[90:105].strip())
			#campo 13 - numero total de posicoes descobertas. Em unidades da opcao
			registro.descobertas = int(line[105:120].strip())
			#campo 14 - posicao total de opcoes no mercado. Em unidades da opcao
			registro.total_opcoes = int(line[120:135].strip())
			#campo 15 - quant de clientes titulares.
			registro.total_titulares = int(line[135:142].strip())
			#campo 16 - quant de clientes lancadores
			registro.total_lancador = int(line[142:149].strip())
			#campo 17 - nao sei o que e
			registro.distribuicao = line[149:152]
			#campo 18 - naoi sei o que e
			registro.estilo = line[152]
			#campo 19 - tipo de ativo nosistema isin norma ISO 6166
			registro.isin = line[153:160].strip()
			self.registros.append(registro)
			#contador de numero da linha
			i += 1
			
		f.close()

if __name__ == '__main__':
	"""
	Executa os script de parse do arquivo de Posicoes em aberto no mercado de Opções da BMF/Bovespa.
	http://www.bmfbovespa.com.br/opcoes/opcoes.aspx?idioma=pt-br&aba=tabVctoOpcoes
	"""
	if(len(sys.argv) != 3):
		sys.stderr.write('\nPasse dois parametros: arquivo a ser lido e arquivo a ser salvo\n')
		sys.stderr.write('Exemplo:\n$python bovespa_parser.py BOV_OP.DAT MEU_CSV.CSV\n')
		sys.exit(1)
	FILE_TEST_PATH = sys.argv[1]
	parser = OpcoesAbertoParser(FILE_TEST_PATH)
	parser.save2csv(sys.argv[2])
	